'''
*****-----*****
This is a simple SVM-based text classification scripts.
This system is designed for non-programmers who are exposed to text classification problems, but
are not sure how to run good performing text classification scripts.

I wrote this script as part of a workshop for non-programmers at the Department of Biomedical Informatics, Arizona State
University.

- The implementation here is a simple Support Vector Machines.
- The approach is designed to be simple while also ensuring very good performance.
- The script simply uses n-gram features and optimizes an SVM classifier using the n-gram features.
- The performance could be improved by adding more features, but the goal here is to use a minimalist
 approach to solve the problem (with little loss in performance) so that non-programmers can download
 and use the script.
- Ofcourse the script can be extended to add more features.

For a more thorough approach to text classification (particularly social media text classification with
imbalanced data), see the following paper and code:

Sarker A and Gonzalez G. Portable automatic text classification for adverse drug reaction detection via multi-corpus
training. J Biomed Inform. 2015 Feb;53:196-207. doi: 10.1016/j.jbi.2014.11.002. Epub 2014 Nov 8.

Source code available at: https://bitbucket.org/asarker/adrbinaryclassifier

*****-----*****
Instructions:

Format:
Libraries needed are:
sklearn
python
nltk
numpy

The python anaconda distribution contains all these libraries.

This script assumes that the data has the following format:

id [tab] class [tab] text

In this example we use the movie review data set, which has the above format. The data set is available at:
https://www.kaggle.com/c/word2vec-nlp-tutorial/data

The first line of this specific data set contains column headers, which needs to be removed for this script.
If the column numbers are different in the chosen data set, please update the lines between 77-85.
In particular, items[x] -> where x represents the column number, starting from 0.

To run, place the txt file in the same folder as the script.

command:
python runcrossvalidation.py [textfilename]

For example, if the text file name is labeledTrainData.txt

python runcrossvalidation.py labeledTrainData.txt

Output:
Once the script is done running (which will take a while), it will print out the best values for c, weights and the best accuracy.


@author: asarker

'''

from __future__ import division
import pandas as pd
from string import strip
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm, cross_validation
from nltk.stem.porter import *
from nltk.corpus import stopwords
import numpy as np
import sys

st = stopwords.words('english')
stemmer = PorterStemmer()
import codecs


def loadDataAsDataFrame(f_path):
    '''
        Given a path, loads a data set and puts it into a dataframe
        - simplified mechanism
    '''
    datalist = []
    count = 0
    infile = codecs.open(f_path, 'r', encoding='utf-8')
    for line in infile:
        items = line.split('\t')
        if len(items) > 2:
            instance_dict = {}

            instance_dict['id'] = items[0]
            instance_dict['class'] = items[1]
            try:
                instance_dict['text'] = strip(items[2].decode('iso-8859-1').encode('utf8'))
            except UnicodeEncodeError:
                ptext = items[2]
                instance_dict['text'] = strip(ptext)
            datalist.append(instance_dict)
            count += 1
    return pd.DataFrame(datalist)


def stem_and_tokenize(raw_text):
    '''
        Function that simply stems the terms using the porter stemmer
        and tokenizes them.
    '''
    words = [stemmer.stem(w) for w in raw_text.lower().split()]
    return (" ".join(words))


if __name__ == '__main__':
    if len(sys.argv)>1:
        f_path = sys.argv[1]
    else:
        f_path = 'labeledTrainData.txt'

    # load the dataset
    training_data = loadDataAsDataFrame(f_path)
    # obtain the texts
    training_data_texts = training_data['text']

    # prepare the vectorizer that generates one-hot vectors. trigrams are generated, which is generally good for text classification.
    # change the max_features number to vary performance..
    vectorizer = CountVectorizer(ngram_range=(1, 3), analyzer="word", tokenizer=None, preprocessor=None, max_features=5000)

    # process the training data using the predefined function..
    tokens = []
    processed_training_data = []
    for t in training_data_texts:
        toks = stem_and_tokenize(t)

        processed_training_data.append(toks)
    trained_data_vectors = vectorizer.fit_transform(processed_training_data).toarray()

    score_dict = {}
    # the program iterates through
    #keeping it simple; could use grid search from sklearn
    cost_vec = [0.125, 0.25, 0.5, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]
    # specify the number of folds
    folds = 10
    class_list = list(training_data['class'])
    class_names = list(set(class_list))
    class_name_counts = {}
    class_weights = {}
    #count the number of times each class occurs
    for c in class_names:
        class_name_counts[c] = class_list.count(c)
    #print the count for each class
    for k in class_name_counts.keys():
        print k, '\t', class_name_counts[k]
    #the weight for a class will be = #totalinstances/#instances of this class
    for k in class_name_counts.keys():
        class_weights[k] = sum(class_name_counts.values()) / class_name_counts[k]

    stdev = np.std(class_weights.values())
    interval = stdev * stdev
    print 'interval ' + str(interval)
    weights = []

    # get the larger weight (smaller class)
    smaller_class_weight = max(class_weights.values())
    # get the smaller weight (larger class)
    bigger_class_weight = min(class_weights.values())
    print 'smaller class weight'
    print smaller_class_weight
    print 'bigger class weight'
    print bigger_class_weight
    # get the class name with the smaller and bigger weight
    smaller_class = [k for k, v in class_weights.items() if v == smaller_class_weight][0]
    bigger_class = [k for k, v in class_weights.items() if v == bigger_class_weight][0]
    weight_range = np.arange(smaller_class_weight - (2 * interval), smaller_class_weight + (2 * interval), interval / 2)
    print 'interval'
    print interval

    print 'weight range '
    print weight_range

    print 'computing optimal parameters... THIS MAY TAKE A LONG TIME..'

    best_accuracy = 0.0
    best_c = -1
    best_weight = -1

    for c in cost_vec:
        for w in weight_range:
            #print 'Running config for.. cost = ' + str(c) + ' weight = ' + str(w) + '...'
            svm_classifier = svm.SVC(C=c, cache_size=200,
                                     class_weight={smaller_class: w, bigger_class: bigger_class_weight},
                                     coef0=0.0, degree=3,
                                     gamma='auto', kernel='rbf', max_iter=-1, probability=True, random_state=None,
                                     shrinking=True, tol=0.001, verbose=False)
            scores = cross_validation.cross_val_score(svm_classifier, trained_data_vectors, training_data['class'],
                                                      n_jobs=-1, cv=folds, scoring='accuracy')
            if scores.mean() > best_accuracy:
                best_accuracy = scores.mean()
                best_c = c
                best_weight = w
    print 'best accuracy : ' + str(best_accuracy)
    print 'best value for c: ' + str(best_c)
    print 'weight for smaller class: ' + str(best_weight) + ' and best weight for larger class: ' + str(bigger_class_weight)

    #write the values to a file so that runclassifier_2_class.py can use the file to classify test data
    outfile = open('best_params.txt','w')
    outfile.write('cost\t'+str(c)+'\n')
    outfile.write(smaller_class+'\t'+str(w)+'\n')
    outfile.write(bigger_class+'\t'+str(bigger_class_weight)+'\n')
    outfile.close()