##Simple, self-optimizing SVM classifier##

This is a simple text classification system, designed particularly for non-experts who are exposed to text classification problems.

The scripts were written as part of workshops conducted at Arizona State University for Biomedical Informatics grad students.


Technical details are in the *TechnicalNotesvXX.pdf* file within the [source](https://bitbucket.org/asarker/simpletextclassifier/src/) page.
Also available here: http://diego.asu.edu/Publications/Textclassif_workshop_v2-3.pdf