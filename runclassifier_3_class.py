'''
*****-----*****
This is a simple SVM-based text classification scripts.
This system is designed for non-programmers who are exposed to text classification problems, but
are not sure how to run good performing text classification scripts.

I wrote this script as part of a workshop for non-programmers at the Department of Biomedical Informatics, Arizona State
University.

- The implementation here is a simple Support Vector Machines.
- The approach is designed to be simple while also ensuring very good performance.
- The script simply uses n-gram features and optimizes an SVM classifier using the n-gram features.
- The performance could be improved by adding more features, but the goal here is to use a minimalist
 approach to solve the problem (with little loss in performance) so that non-programmers can download
 and use the script.
- Ofcourse the script can be extended to add more features.

For a more thorough approach to text classification (particularly social media text classification with
imbalanced data), see the following paper and code:

Sarker A and Gonzalez G. Portable automatic text classification for adverse drug reaction detection via multi-corpus
training. J Biomed Inform. 2015 Feb;53:196-207. doi: 10.1016/j.jbi.2014.11.002. Epub 2014 Nov 8.
Source code available at: https://bitbucket.org/asarker/adrbinaryclassifier

*****-----*****
Instructions:

Format:
Libraries needed are:
sklearn
python
nltk
numpy

The python anaconda distribution contains all these libraries.

This script assumes that the data has the following format:

id [tab] class [tab] text

In this example we use the movie review data set, which has the above format. The data set is available at:
https://www.kaggle.com/c/word2vec-nlp-tutorial/data

The first line of this specific data set contains column headers, which needs to be removed for this script.
If the column numbers are different in the chosen data set, please update the lines between 77-85.
In particular, items[x] -> where x represents the column number, starting from 0.

To run, place the txt file in the same folder as the script.

command:
python runcrossvalidation.py [textfilename]

For example, if the text file name is labeledTrainData.txt

python runcrossvalidation.py labeledTrainData.txt

Output:
Once the script is done running (which will take a while), it will print out the best values for c, weights and the best accuracy.


@author: asarker

'''

from __future__ import division
import pandas as pd
from string import strip
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import svm, cross_validation
from nltk.stem.porter import *
from nltk.corpus import stopwords
import numpy as np
import sys

st = stopwords.words('english')
stemmer = PorterStemmer()
import codecs


def loadDataAsDataFrame(f_path):
    '''
        Given a path, loads a data set and puts it into a dataframe
        - simplified mechanism
    '''
    datalist = []
    count = 0
    infile = codecs.open(f_path, 'r3', encoding='utf-8')
    for line in infile:
        items = line.split('\t')
        if len(items) > 2:
            instance_dict = {}

            instance_dict['id'] = items[0]
            instance_dict['class'] = items[1]
            try:
                instance_dict['text'] = strip(items[2].decode('iso-8859-1').encode('utf8'))
            except UnicodeEncodeError:
                ptext = items[2]
                instance_dict['text'] = strip(ptext)
            datalist.append(instance_dict)
            count += 1
    return pd.DataFrame(datalist)


def stem_and_tokenize(raw_text):
    '''
        Function that simply stems the terms using the porter stemmer
        and tokenizes them.
    '''
    words = [stemmer.stem(w) for w in raw_text.lower().split()]
    return (" ".join(words))


if __name__ == '__main__':
    paramsinfile = open('best_params.txt')
    train_f_path =''
    test_f_path = ''
    if len(sys.argv)>2:
        train_f_path = sys.argv[1]
        test_f_path = sys.argv[2]
    else:
        f_path = 'labeledTrainData'

    cost = float(strip(paramsinfile.readline().split()[1]))
    weights = {}
    for line in paramsinfile:
        items = line.split('\t')
        class_ = items[0]
        weight = strip(items[1])
        weights[class_]=weight

    # load the dataset
    training_data = loadDataAsDataFrame(train_f_path)
    # obtain the texts
    training_data_texts = training_data['text']

    # prepare the vectorizer that generates one-hot vectors. trigrams are generated, which is generally good for text classification.
    # change the max_features number to vary performance..
    vectorizer = CountVectorizer(ngram_range=(1, 3), analyzer="word", tokenizer=None, preprocessor=None, max_features=5000)

    # process the training data using the predefined function..
    tokens = []
    processed_training_data = []
    for t in training_data_texts:
        toks = stem_and_tokenize(t)

        processed_training_data.append(toks)
    trained_data_vectors = vectorizer.fit_transform(processed_training_data).toarray()

    test_data = loadDataAsDataFrame(test_f_path)
    test_data_texts = test_data['text']
    tokens = []
    processed_test_data = []
    for t in test_data_texts:
        toks = stem_and_tokenize(t)
        processed_test_data.append(toks)
    test_data_vectors = vectorizer.transform(processed_test_data).toarray()

    sorted_weights = sorted(class_weights.values())
    smaller_class_weight = sorted_weights[0]
    middle_class_weight = sorted_weights[1]
    bigger_class_weight = sorted_weights[2]

    print 'smaller class weight'
    print smaller_class_weight
    print 'middle class weight'
    print middle_class_weight
    print 'bigger class weight'
    print bigger_class_weight

    # get the class name with the smaller and bigger weight
    smaller_class = [k for k, v in class_weights.items() if v == smaller_class_weight][0]
    middle_class = [k for k, v in class_weights.items() if v == middle_class_weight][0]
    bigger_class = [k for k, v in class_weights.items() if v == bigger_class_weight][0]

    #best_c = -1
    svm_classifier = svm.SVC(C=cost, cache_size=200, class_weight={smaller_class: smaller_class_weight, bigger_class: bigger_class_weight, middle_class: middle_class_weight},
                             coef0=0.0, degree=3, gamma='auto', kernel='rbf', max_iter=-1, probability=True,
                             random_state=None, shrinking=True, tol=0.001, verbose=False)
    svm_classifier = svm_classifier.fit( trained_data_vectors, training_data["class"] )
    result = svm_classifier.predict(test_data_vectors)

    outfile = open('classificationresults.txt','w')

    for id_,class_,text in zip(test_data['id'],result,test_data['text']):
        line = id_ + '\t' + str(class_) + '\t' + text
        line = line.encode('utf8')
        try:
            outfile.write(line+'\n')
        except UnicodeEncodeError:
            print '** There was an error writing the result for '+ id_+' which has classification: ' + str(class_)
    print 'Results written to file: classificationresults.txt'
    outfile.close()