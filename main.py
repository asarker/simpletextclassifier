'''

To run:

python main.py [trainingfile] [testfile]

Example:

python main.py labeledTrainData.txt testData.txt

To run 10-fold CV on training data only:

python main.py labeledTrainData.txt

'''

import sys, os

if __name__ == '__main__':

    dir_path = os.path.dirname(os.path.realpath(__file__))
    print 'Current working directory: ' + dir_path
    if len(sys.argv) == 1:
        print 'Training file name missing'
        print 'Input requires at least the training document name'
        print 'Format: python main.py [trainingfilename] [testfilename]'
    if len(sys.argv) == 2:
        training_filename = sys.argv[1]
        print 'File path given: ' + dir_path + '/' + training_filename
        if os.path.isfile(dir_path + '/' + training_filename):
            os.system('python ' + dir_path + '/runcrossvalidation.py ' + training_filename)
            print 'The best parameters have been saved in best_params.txt'
        else:
            print 'There is no file named ' + training_filename +' in the current directory..'
            print 'Please recheck filename and try again.. '

    if len(sys.argv) == 3:
        training_filename = sys.argv[1]
        test_filename = sys.argv[2]
        print 'Training file path given: ' + dir_path + '/' + training_filename
        print 'Test file path given: ' + dir_path + '/' + test_filename
        if os.path.isfile(dir_path + '/' + training_filename) and os.path.isfile(dir_path + '/' + test_filename):
            os.system('python ' + dir_path + '/runcrossvalidation.py ' + training_filename)
            os.system('python ' + dir_path + '/runclassifier.py ' +training_filename + ' '+ test_filename)
        else:
            print 'Either ' + training_filename +' or ' + test_filename + ' or both are missing from the current directory..'
            print 'Please recheck filename and try again.. '
